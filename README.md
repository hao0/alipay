# Alipay

轻量的支付宝组件(A Lightweight Alipay Component)
---

+ 包引入:
	
	```xml
	<dependency>
        <groupId>me.hao0</groupId>
        <artifactId>alipay-core</artifactId>
        <version>1.0.10</version>
    </dependency>
	```
+ 具体文档见<a target="_blank" href="https://github.com/ihaolin/alipay">这里</a>。
        